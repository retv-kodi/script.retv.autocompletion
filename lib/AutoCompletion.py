# -*- coding: utf8 -*-

# Copyright (C) 2015 - Philipp Temminghoff <phil65@kodi.tv>
# This program is Free Software see LICENSE file for details

import xbmcaddon
import xbmcvfs
import urllib
import codecs
import os
import time
import hashlib
import urllib2
import xbmc
import simplejson
import json

ADDON = xbmcaddon.Addon()
SETTING = ADDON.getSetting
ADDON_PATH = os.path.join(os.path.dirname(__file__), "..")
ADDON_ID = ADDON.getAddonInfo('id')
ADDON_DATA_PATH =  os.path.join(xbmc.translatePath('special://temp/').decode("utf-8"),"search")
#ADDON_DATA_PATH = xbmc.translatePath("special://profile/addon_data/%s" % ADDON_ID).decode("utf-8")
HEADERS = {'User-agent': 'Mozilla/5.0'}

TMDB_IMAGE_URL = "https://image.tmdb.org/t/p/w185/"

def get_lang():
    lang = SETTING("autocomplete_lang_local")
    if not lang:
        lang = "en"
        
    return lang

def log(txt):
    return
    if isinstance(txt, str):
        txt = txt.decode("utf-8", 'ignore')
    message = u'%s: %s' % (ADDON_ID, txt)
    xbmc.log(msg=message.encode("utf-8", 'ignore'))


def get_autocomplete_items(search_str, limit=10, provider_name=None):
    """
    get dict list with autocomplete
    """
    
    if not provider_name:
        provider_name = SETTING("autocomplete_provider")
    
    #xbmc.log( "Setting is : "+provider_name)
    if xbmc.getCondVisibility("System.HasHiddenInput"):
        return []
    if provider_name == "youtube":
        provider = GoogleProvider(youtube=True, limit=limit)
    elif provider_name == "google":
        provider = GoogleProvider(limit=limit)
    elif provider_name == "bing":
        provider = BingProvider(limit=limit)
    elif provider_name == "tmdb":
        provider = TMDBProvider(limit=limit)
    else:
        provider = LocalDictProvider(limit=limit)
    provider.limit = limit
    return provider.get_predictions(search_str)


class BaseProvider(object):

    def __init__(self, *args, **kwargs):
        self.limit = kwargs.get("limit", 10)

    def get_predictions(self, search_str):
        pass

    def prep_search_str(self, text):
        if type(text) != unicode:
            text = text.decode('utf-8')
        for chr in text:
            if ord(chr) >= 1488 and ord(chr) <= 1514:
                return text[::-1]
        return text

    def get_prediction_listitems(self, search_str):
        for item in self.get_predictions(search_str):
            li = {"label": item,
                  "search_string": search_str}
            yield li


class GoogleProvider(BaseProvider):

    def __init__(self, *args, **kwargs):
        super(GoogleProvider, self).__init__(*args, **kwargs)
        self.youtube = kwargs.get("youtube", False)

    def get_predictions(self, search_str):
        """
        get dict list with autocomplete labels from google
        """
        if not search_str:
            return []
        items = []
        result = self.fetch_data(search_str)
        for i, item in enumerate(result):
            li = {"label": item,
                  "search_string": self.prep_search_str(item)}
            items.append(li)
            if i > self.limit:
                break
        return items

    def fetch_data(self, search_str):
        base_url = "http://clients1.google.co.in/complete/"
        url = "search?hl=%s&q=%s&json=t&client=serp" % (get_lang(), urllib.quote_plus(search_str))
        if self.youtube:
            url += "&ds=yt"
        result = get_JSON_response(url=base_url + url,
                                   headers=HEADERS,
                                   folder="Google")
        if not result or len(result) <= 1:
            return []
        else:
            return result[1]


class BingProvider(BaseProvider):

    def __init__(self, *args, **kwargs):
        super(BingProvider, self).__init__(*args, **kwargs)

    def get_predictions(self, search_str):
        """
        get dict list with autocomplete labels from bing
        """
        if not search_str:
            return []
        items = []
        result = self.fetch_data(search_str)
        for i, item in enumerate(result):
            li = {"label": item,
                  "search_string": self.prep_search_str(item)}
            items.append(li)
            if i > self.limit:
                break
        return items

    def fetch_data(self, search_str):
        base_url = "http://api.bing.com/osjson.aspx?"
        url = "query=%s" % (urllib.quote_plus(search_str))
        result = get_JSON_response(url=base_url + url,
                                   headers=HEADERS,
                                   folder="Bing")
        if not result:
            return []
        else:
            return result[1]

            
class TMDBProvider(BaseProvider):

    def __init__(self, *args, **kwargs):
        super(TMDBProvider, self).__init__(*args, **kwargs)

    def get_predictions(self, search_str):
        """
        get dict list with autocomplete labels from TMDB
        """
        if not search_str:
            return []
        items = []
        result = self.fetch_data(search_str)
        for i, item in enumerate(result):

            try:
                name = item[u'title']
            except:
                name = item[u'name']
            
            try:
                pic = TMDB_IMAGE_URL+item[u'poster_path']
            except:
                pic = ""

            try:
                year = item[u'release_date'][:4]
            except:
                try:
                    year = item[u'first_air_date'][:4]
                except:
                    year = ""

            li = {"label": name,
                  "Label2": item[u'media_type'],
                  "thumb": pic,
                  "pic": pic,
                  "year": year,
                  "DBID": item[u'id']}
            
            items.append(li)
            if i > self.limit:
                break
        return items

    def fetch_data(self, search_str):
        base_url = "https://api.themoviedb.org/3/search/multi?api_key=d272326e467344029e68e3c4ff0b4059&language=en-US&"
        url = "query=%s" % (urllib.quote_plus(search_str))
        result = get_JSON_response(url=base_url + url,
                                   headers=HEADERS,
                                   folder="TMDB")
                                   
        #xbmc.log(str(result))
        #xbmc.log(str(result.keys()))
        if not result:
            return []
        else:
            return result[u'results']
    
            
class LocalDictProvider(BaseProvider):

    def __init__(self, *args, **kwargs):
        super(LocalDictProvider, self).__init__(*args, **kwargs)

    def get_predictions(self, search_str):
        """
        get dict list with autocomplete labels from locally saved lists
        """
        listitems = []
        k = search_str.rfind(" ")
        if k >= 0:
            search_str = search_str[k + 1:]
        path = os.path.join(ADDON_PATH, "resources", "data", "common_%s.txt" % get_lang())
        with codecs.open(path, encoding="utf8") as f:
            for i, line in enumerate(f.readlines()):
                if not line.lower().startswith(search_str) or len(line) <= 2:
                    continue
                li = {"label": line,
                      "search_string": line}
                listitems.append(li)
                if len(listitems) > self.limit:
                    break
        return listitems


def get_JSON_response(url="", cache_days=2.0, folder=False, headers=False):
    """
    get JSON response for *url, makes use of file cache.
    """
    now = time.time()
    hashed_url = hashlib.md5(url).hexdigest()
    if folder:
        cache_path = xbmc.translatePath(os.path.join(ADDON_DATA_PATH, folder))
    else:
        cache_path = xbmc.translatePath(os.path.join(ADDON_DATA_PATH))
    path = os.path.join(cache_path, hashed_url + ".txt")
    cache_seconds = int(cache_days * 86400.0)
    if xbmcvfs.exists(path) and ((now - os.path.getmtime(path)) < cache_seconds):
        results = read_from_file(path)
        log("loaded file for %s. time: %f" % (url, time.time() - now))
    else:
        response = get_http(url, headers)
        try:
            results = simplejson.loads(response)
            log("download %s. time: %f" % (url, time.time() - now))
            save_to_file(results, hashed_url, cache_path)
        except:
            log("Exception: Could not get new JSON data from %s. Tryin to fallback to cache" % url)
            log(response)
            if xbmcvfs.exists(path):
                results = read_from_file(path)
            else:
                results = []
    if results:
        return results
    else:
        return []


def get_http(url=None, headers=False):
    """
    fetches data from *url, returns it as a string
    """
    succeed = 0
    if not headers:
        headers = {'User-agent': 'ReTV/16.1 ( support@retv.in )'}
    request = urllib2.Request(url)
    for (key, value) in headers.iteritems():
        request.add_header(key, value)
    while (succeed < 2) and (not xbmc.abortRequested):
        try:
            response = urllib2.urlopen(request, timeout=3)
            data = response.read()
            return data
        except:
            log("get_http: could not get data from %s" % url)
            xbmc.sleep(1000)
            succeed += 1
    return None


def read_from_file(path="", raw=False):
    """
    return data from file with *path
    """
    if not xbmcvfs.exists(path):
        return False
    try:
        with open(path) as f:
            log("opened textfile %s." % (path))
            if raw:
                return f.read()
            else:
                return simplejson.load(f)
    except:
        log("failed to load textfile: " + path)
        return False



def save_to_file(content, filename, path=""):
    """
    dump json and save to *filename in *path
    """
    if not xbmcvfs.exists(path):
        xbmcvfs.mkdirs(path)
    text_file_path = os.path.join(path, filename + ".txt")
    now = time.time()
    text_file = xbmcvfs.File(text_file_path, "w")
    simplejson.dump(content, text_file)
    text_file.close()
    log("saved textfile %s. Time: %f" % (text_file_path, time.time() - now))
    return True
